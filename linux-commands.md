**linux-commands**
https://www.redhat.com/sysadmin/one-line-linux-commands

- **# daemon-reload**
- systemctl daemon-reload
- 
- **# Disable firewalld**
- sudo systemctl disable firewalld --now
- 
- **# Disable nm-cloud-setup if exists and enabled**
- sudo systemctl disable nm-cloud-setup.service nm-cloud-setup.timer
- sudo reboot

**- #change SELinux mode to "permissive"**
- Run the command:
- 
- # setenforce 0
- 
**- Check the current status of SELinux:**
- 
- # sestatus | grep "Current mode"
- Current mode: permissive

- To set SELinux back to "enforcing", run
- # setenforce 1

===============================================================

- # vi /etc/selinux/config
- 
- Change the SELINUX value to:
- 
- disabled to completely turn off SELinux on the server:
- 
- SELINUX=disabled
- 
- permissive to make SELinux print warnings instead of enforce security policy:
- 
- SELINUX=permissive
- 
- enforcing to make SELinux security policy enforced:
- 
- SELINUX=enforcing
==================================================================

**- Restart the server to apply the changes.**
- 
- Check the permanent status of SELinux:
- 
- # sestatus | grep permissive
--------------------------------------------------------------











